# Summary

[Introduction](./intro.md)

# Notes

- [Docker](./day1/notes.md)
- [CI/CD](./day2/notes.md)
- [Github Actions](./day3/notes.md)

# Bonus

- [CI/CD on Gitlab](./xxx_bonus/notes.md)

# Code

- [Express Example](./xxx_code/1_nodeexpress/code.md)
- [React Example](./xxx_code/2_react/code.md)
- [SQL Example](./xxx_code/3_nodesql/code.md)
- [mdBook Example](./xxx_code/4_mdbook/code.md)
