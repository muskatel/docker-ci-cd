# Docker

## Lesson Outline

**Introduction to Docker**: Begin the lesson by introducing the concept of Docker and its importance in modern software development. Explain how Docker allows developers to package and distribute applications in a portable and consistent way. Discuss the benefits of using Docker, such as reducing the "it works on my machine" problem, being able to isolate and manage dependencies, and being able to easily scale applications horizontally.

**Docker Architecture**: Next, cover the key elements of the Docker architecture, including the Docker daemon, client, image, and container. Provide examples of how each component works and how they interact with one another.

**Docker commands and operations**: Introduce students to the basic Docker commands and operations, such as docker run, docker pull, docker push, docker start, and docker stop. Demonstrate how these commands are used to manage images and containers.

**Hands-on practice**: After the students have an understanding of the theory, you can divide them into groups and give them an example of a simple project and let them create a Dockerfile and use it to containerize their application. You could use a combination of theory and practical examples, to help them solidify their understanding.

**Advanced topics**: Finally, you could cover some more advanced topics, such as using Docker Compose to define and run multi-container applications and using Docker Swarm or Kubernetes to orchestrate and manage containers at scale.

**Conclusion**: Summarize the key points and discuss the importance of Docker in software development and how it can help to easily distribute, run and scale applications, and how it can be used in the context of a bigger CI/CD pipeline.

## Learning Objectives


https://fireship.io/lessons/docker-basics-tutorial-nodejs/

## Docker Installation

It is possible to install and run docker from WSL but since WSL2 the best practice is to use the Docker Desktop for Windows, since it brings native support for the Docker and make it run faster, and it also includes integration with WSL2.

Download Docker from the [official website](https://www.docker.com/products/docker-desktop)

## How is Docker different?

In Virtualization, a Hypervisor such as VMware or VirtualBox is installed on a host operating system, and it creates and manages virtualized environments, or virtual machines (VMs), that run on top of the host operating system. Each VM runs its own operating system, and applications run inside the VM as if they were running on a dedicated physical machine. This method requires all the resources of the virtual machine to be allocated to the guest operating system, including memory and CPU cycles, even if they're not being used by the applications running on it.

In contrast, Docker uses a containerization approach, where it isolates applications and their dependencies into lightweight, portable containers that can run on any host operating system. Containers share the host operating system kernel and only include the libraries and system resources required to run the application. This allows for more efficient use of resources, as multiple containers can run on the same host operating system, each with its own isolated resources. Additionally, Because of their small size and consistent behaviour, containers are much easier to distribute, and it's simpler to deploy, test, and scale applications in containers than it is with VMs.

Another important difference is that, a container can be launched from an image and that image is an executable package, which contains everything needed to run the application, including the code, runtime, libraries, environment variables, and config files, so it is a lot more lightweight and consistent than using traditional virtual machines.

We will start by creating an application on top of an existing image, but in later examples we will create our own immutable image using GitHub Actions.

## Setting up a Node based Dockerfile

1. Create a file at the root directory of your project called `Dockerfile` (No file extension) with the following text

```
FROM node:14-alpine

# Create a working directory
WORKDIR /app

# Copy package.json and package-lock.json (if you use npm) or yarn.lock (if you use yarn)
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the application code
COPY . .

# Expose the port that the application will run on
EXPOSE 3000

# Start the application
CMD [ "npm", "start" ]
```

`FROM` specifies the Docker image which we will use for our application. A list of available images can be found on [here](https://hub.docker.com/)

`ADD` instructs Docker to copy the .jar artifact created by our project in the target folder to the root directory of our application

`ENTRYPOINT` describes the command which Docker will run on our .jar file after it has been copied

2. Run the following command in the terminal:

```bash
docker build -t <your-image-name> .
```

3. To run the instance of your Docker image, enter the following command into the terminal:

```bash
$ docker run -p <port-number>:<port-number> <your docker tag name here>
```

We have now successfully created and run a Docker container. To list all running containers or all existing containers along with relevant information about them, use the following commands:

```bash
docker ps
docker ps -a
```

Docker's desktop dashboard gives us the option of stopping and starting the instances of our containers. It is the equivalent of running the following commands from the terminal:

```bash
$ docker container start <container id>
$ docker container stop <container id>
```

> Your IDE of choice may have a Docker plug-in which will also give you easy access to these controls.

## Docker Images and Containers

When we specified the following in the `Dockerfile`:

```Dockerfile
FROM node:14-alpine
```

We told Docker that our application needs Node to run, this is the _base_ image.
We added our application on top of this image, but the result was a _new_ docker image that is now our application.

Because the `Node` image is immutable (we cannot make changes to it), we don't actually need to copy it around with our application. When  we deploy our application somewhere, the host will download the base `Node` image for itself. 

When our application is started up (running) the host creates a container for it to run inside of, this is a **COPY** of the image.

This is a very efficient way to deploy servers, and allows for multiple instance to be run very easily, but it does create some complication. One that you will encounter quickly is that you cannot make local file changes that save permanently. 

## Resources

[Fireship IO - Docker Basics](https://fireship.io/lessons/docker-basics-tutorial-nodejs/)

---

Notes compiled by Craig Marais ([@muskatel](https://gitlab.com/muskatel)). Copyright (c) 2023 Craig Marais