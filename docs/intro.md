# Introduction

## Sample code explained

For this project we will use _four_ simple projects.

These are as follows:
- Node & Express - "Hello, World!"
- React 
- Node & SQL 
- mdBook 

> Use the links in the sidebar to access these code samples.



---

Notes compiled by Craig Marais ([@muskatel](https://gitlab.com/muskatel)). Copyright (c) 2023 Craig Marais