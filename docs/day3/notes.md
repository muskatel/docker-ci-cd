# GitHub Actions

## Lesson overview

**Introduction to GitHub Actions**: Begin the lesson by introducing the concept of GitHub Actions and its importance in modern software development. Explain how GitHub Actions allows developers to automate software development workflows, such as building, testing, and deploying code. Discuss the benefits of using GitHub Actions, such as the ability to integrate with other services and tools, the ability to run workflows in response to specific events, and the ability to easily share and reuse workflows.

**GitHub Actions Workflow**: Next, cover the key elements of a GitHub Actions workflow, including events, jobs, and steps. Explain how events trigger workflows, how jobs are used to perform actions, and how steps are used to define the actions that a job should perform. Provide examples of how each component works and how they interact with one another.

**Hands-on practice**: After the students have an understanding of the theory, you can divide them into groups and give them an example of a simple project and let them create a workflow using GitHub Actions. You could use a combination of theory and practical examples, to help them solidify their understanding.

**Advanced topics**: Finally, you could cover some more advanced topics, such as using GitHub Actions with other services and tools, and creating custom actions. You could also show them how to use GitHub Actions to deploy and manage their application on different platforms like AWS, Azure, or GCP.

**Conclusion**: Summarize the key points and discuss the importance of GitHub Actions in software development and how it can be integrated in a bigger CI/CD pipeline, and how it can help to automate repetitive tasks, increase collaboration among team members, and increase the speed of releases.

## GitHub Actions Overview

GitHub Actions is a powerful automation tool that can be used to automate a wide range of tasks in your development workflow, such as building, testing, and deploying code.

GitHub Actions has parts:

**Workflows**: Workflows are the heart of GitHub Actions. They define the steps that are executed in response to a specified event, such as a push to a specific branch. Workflows are defined using YAML files that are stored in the .github/workflows directory in your repository.

**Events**: Events are the triggers that start a workflow. Examples of events include pushes to a specific branch, pull request events, or the creation of a new release. You can specify the events that trigger a workflow in the workflow definition file.

**Actions**: Actions are the individual steps that are executed in a workflow. They are defined as individual tasks that can be reused across workflows, making it easy to share common functionality across projects. You can use actions created by GitHub or the community, or create your own actions by using a Docker container to run your custom script.

**Jobs**: Jobs are a set of steps that are run in parallel or sequentially. Each job runs on a specific environment and can be composed of one or more steps.

**Steps**: Steps are the atomic building blocks of a job. They can be individual commands or calls to an action. Each step runs in its own container and can be configured to run commands, set environment variables, and manage dependencies.

**Environments**: Environments are the infrastructure that runs your jobs and steps. Each job can run on a specific environment, such as a specific operating system or runtime version. You can specify the environment of a job in the workflow definition file.

**Secrets**: Secrets are encrypted environment variables that allow you to securely share sensitive information, such as API keys, with your workflows. You can use secrets to authenticate with external services, encrypt sensitive data, and more

These are the main parts of GitHub Actions, but there are many other features such as environment matrix, matrix strategies, and so on.

By using GitHub Actions, you can automate a wide range of tasks and build a highly-customizable, efficient, and secure workflow for your project.

## Guide to Using GitHub Actions

Here's an example of how you might migrate an existing Node.js project and Dockerfile to a GitHub Actions solution:

1. First, you'll need to create a new repository on GitHub for your Node.js project. You can do this by going to the GitHub website, and clicking the "+" button in the top-right corner, and then selecting "New repository".

2. Next, you can move your existing Node.js project files and the Dockerfile to the local repository folder and push it to the GitHub repository you just created.

3. Once your code is in GitHub, you can set up GitHub Actions to automate the build, test, and deploy process. To do this, you'll need to create a new file in the root of your repository called .github/workflows/ci.yml. This file will contain the workflow definition that GitHub Actions uses to run your pipeline. Here's an example pipeline definition that you can use as a starting point:

```yml
name: CI

on:
  push:
    branches:
      - main

jobs:
  build:
    runs-on: ubuntu-latest

    steps:
    - name: Checkout code
      uses: actions/checkout@v2
    
    - name: Install dependencies
      run: npm ci
    
    - name: Build and test the application
      run: npm run build && npm test
      
    - name: Build the Docker image
      run: docker build -t your-image-name .
      
    - name: Push the image to the container registry
      run: docker push your-image-name
```

This pipeline will execute on every push to the main branch, installing dependencies, building the application, and running tests. Then it will build the Docker image and push it to a container registry, you can replace the your-image-name with your own name and the container registry you are using.

4. Once the ci.yml file is in place, you can go to the Actions tab of your repository on GitHub, and you should see the pipeline running. You can monitor the progress and see the logs of each step.

5. Finally, you can set up a deploy step on your pipeline that uses the image to deploy it to a cloud provider like Heroku, AWS Elastic Beanstalk, Google App Engine, or Azure Web App.

## Resources

[GitHub Docs - GitHub Actions](https://docs.github.com/en/actions)



---

Notes compiled by Craig Marais ([@muskatel](https://gitlab.com/muskatel)). Copyright (c) 2023 Craig Marais