# mdBook Example - Very Important Document

[mdBook Documentation](https://rust-lang.github.io/mdBook/)

Start by creating a folder for your solution and moving into it:

```bash
mkdir mdbook_example && cd mdbook_example
```

---

Notes compiled by Craig Marais ([@muskatel](https://gitlab.com/muskatel)). Copyright (c) 2023 Craig Marais