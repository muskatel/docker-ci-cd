# React Example - CV Site

Start by creating a folder for your solution and moving into it:

```bash
mkdir react_example && cd react_example
```

---

Notes compiled by Craig Marais ([@muskatel](https://gitlab.com/muskatel)). Copyright (c) 2023 Craig Marais