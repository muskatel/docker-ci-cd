# Express Example - "Hello, World!"

> Node loves to download an absolute plethora of dependencies (`node_modules`), it is vital that you use an appropriate `.gitignore` file for these projects!

Start by creating a folder for your solution and moving into it:

```bash
mkdir express_example && cd express_example
```

Initialise a new project by calling the init option with `npm`:

```bash
npm init
```

You may use all defaults on the prompts for now.

We will be using the `express` framework, so install that into the current project:

```bash
npm install express
```

At this point, `package.json` should look as follows, notice the `express` dependency:

```json
{
  "name": "express_example",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "",
  "license": "ISC",
  "dependencies": {
    "express": "^4.17.2"
  }
}
```

You will need to edit it to include a start script (notice the extra comma, this is a JSON file after all):

```json
{
  "name": "express_example",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "start": "node index.js",
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "Craig Marais",
  "license": "MIT",
  "dependencies": {
    "express": "^4.17.2"
  }
}
```

Next, create an `index.js` file:

```bash
touch index.js
```

Edit the `index.js` file to the following:

```javascript
const express = require('express') // import express module
const app = express() // create an instance of express app
const port = process.env.PORT || 8080 // get the value of environment variable PORT, or 8080 if it is not set

app.get('/', (req, res) => res.status(200).send('<h1>Hello, World!</h1>')) // create route that handle GET request on root path and returns "Hello World!" message

app.listen(port, () => {
    console.log(`Listening on port ${port}`) // start listening on the specified port and log a message to the console
})

```

Save the file and launch the application by running:

```bash
npm run start
```

> The server runs until you kill it.

You can open you browser to `localhost:8080` and you will see an html tag with `Hello, World!`.

**CONGRATULATIONS** you just built a webserver!

---

Notes compiled by Craig Marais ([@muskatel](https://gitlab.com/muskatel)). Copyright (c) 2023 Craig Marais