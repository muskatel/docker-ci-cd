# SQL Example - Wow, Pets!

Start by creating a folder for your solution and moving into it:

```bash
mkdir sql_example && cd sql_example
```

```bash
npm install mysql2 sequelize sequelize-cli
npx sequelize init
npx sequelize model:generate --name Pet --attributes name:string,age:integer,species:string
```

```json
{
  "development": {
    "username": "your_username",
    "password": "your_password",
    "database": "database_name",
    "host": "host_name",
    "dialect": "mysql"
  }
}
```


```bash
npx sequelize db:migrate
```


```
sql_example/
|--models/
|----pet.js 
|--views/
|----index.html
|----pets.html
|--app.js
|--package.json
|--config.js
```

```bash
npm install express ejs
```

**app.js**:
```javascript

const express = require('express')
const app = express()
const Pet = require('./models/pet')
const bodyParser = require('body-parser')

app.use(bodyParser.urlencoded({ extended: false }))
app.use(express.static(__dirname + '/views'))

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/views/index.html')
})

app.post('/pets', (req, res) => {
  Pet.create({
    name: req.body.name,
    age: req.body.age,
    species: req.body.species
  })
    .then(() => {
      res.redirect('/pets')
    })
    .catch((err) => {
      res.status(500).send('Error: ' + err)
    })
})

app.get('/pets', (req, res) => {
  Pet.findAll()
    .then((pets) => {
      res.render('pets', { pets: pets })
    })
    .catch((err) => {
      res.status(500).send('Error: ' + err)
    })
})

app.listen(3000, () => {
  console.log('Server started at http://localhost:3000')
})
```


**Pets.html**:

```html
!DOCTYPE html>
<html>
  <head>
    <title>Pets</title>
  </head>
  <body>
    <h1>All Pets</h1>
    <ul>
      <% for(let i = 0; i < pets.length; i++) { %>
        <li>
          <strong>Name:</strong> <%= pets[i].name %><br>
          <strong>Age:</strong> <%= pets[i].age %><br>
          <strong>Species:</strong> <%= pets[i].species %><br>
          <br>
          <form action='/pets/update' method='post'>
            <input type='hidden' name='id' value='<%= pets[i].id %>'>
            <label for='name'>Name:</label>
            <input type='text' name='name' value='<%= pets[i].name %>'><br>
            <label for='age'>Age:</label>
            <input type='number' name='age' value='<%= pets[i].age %>'><br>
            <label for='species'>Species:</label>
            <input type='text' name='species' value='<%= pets[i].species %>'><br>
            <input type='submit' value='update'>
          </form>
          <form action='/pets/delete' method='post'>
            <input type='hidden' name='id' value='<%= pets[i].id %>'>
            <input type='submit' value='delete'>
          </form>
          <hr>
        </li>
      <% } %>
    </ul>
  </body>
</html>

```
---

Notes compiled by Craig Marais ([@muskatel](https://gitlab.com/muskatel)). Copyright (c) 2023 Craig Marais