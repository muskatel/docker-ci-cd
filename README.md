# Docker and CI/CD Notes

Online version available here: [![web](https://img.shields.io/static/v1?logo=icloud&message=Online&label=web&color=success)](https://muskatel.gitlab.io/docker-ci-cd/)

Current build: [![pipeline status](https://gitlab.com/muskatel/docker-ci-cd/badges/main/pipeline.svg)](https://gitlab.com/muskatel/docker-ci-cd/-/commits/main)

---

Notes compiled by Craig Marais ([@muskatel](https://gitlab.com/muskatel)). Copyright (c) 2023 Craig Marais